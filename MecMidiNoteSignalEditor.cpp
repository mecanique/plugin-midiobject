/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecMidiNoteSignalEditor.h"

MecMidiNoteSignalEditor::MecMidiNoteSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecMidiSignalEditor(Signal, MainEditor, Parent, F)
{
labelNote = new QLabel(tr("Note:"), widgetProperties);
comboBoxNote = new QComboBox(widgetProperties);
	comboBoxNote->addItems(QStringList() << tr("Do") << tr("Do#") << tr("Ré") << tr("Mi♭") << tr("Mi") << tr("Fa") << tr("Fa♯") << tr("Sol") << tr("La♭") << tr("La") << tr("Si♭") << tr("Si"));
	connect(comboBoxNote, SIGNAL(currentIndexChanged(int)), SLOT(updateName()));
labelOctave = new QLabel(tr("Octave:"), widgetProperties);
spinBoxOctave = new QSpinBox(widgetProperties);
	spinBoxOctave->setRange(-2, 8);
	spinBoxOctave->setValue(3);
	connect(spinBoxOctave, SIGNAL(editingFinished()), SLOT(updateName()));

addGeneralWidget(labelNote);
addGeneralWidget(comboBoxNote);
addGeneralWidget(labelOctave);
addGeneralWidget(spinBoxOctave);

MecMidiNoteSignalEditor::nameElementChanged(signal());
}

MecMidiNoteSignalEditor::~MecMidiNoteSignalEditor()
{
//Rien de particulier.
}
	
QRegExp MecMidiNoteSignalEditor::midiName()
{
return QRegExp("^C([1-9]|1[0-6])_(NoteOff|NoteOn|PolyAfter)_(Do|Dod|Re|Mib|Mi|Fa|Fad|Sol|Lab|La|Sib|Si)([0-8]|y|z)$");
}
	
void MecMidiNoteSignalEditor::nameElementChanged(MecAbstractElement *Element)
{
MecMidiSignalEditor::nameElementChanged(Element);

if (midiName().exactMatch(signal()->elementName()))
	{
	comboBoxNote->blockSignals(true);
	QString note(signal()->elementName().section(QRegExp("(^C)|_"), 3, 3).remove(QRegExp("(z|y|[0-8])$")));
	if (note == "Do") comboBoxNote->setCurrentIndex(0);
	else if (note == "Dod") comboBoxNote->setCurrentIndex(1);
	else if (note == "Re") comboBoxNote->setCurrentIndex(2);
	else if (note == "Mib") comboBoxNote->setCurrentIndex(3);
	else if (note == "Mi") comboBoxNote->setCurrentIndex(4);
	else if (note == "Fa") comboBoxNote->setCurrentIndex(5);
	else if (note == "Fad") comboBoxNote->setCurrentIndex(6);
	else if (note == "Sol") comboBoxNote->setCurrentIndex(7);
	else if (note == "Lab") comboBoxNote->setCurrentIndex(8);
	else if (note == "La") comboBoxNote->setCurrentIndex(9);
	else if (note == "Sib") comboBoxNote->setCurrentIndex(10);
	else if (note == "Si") comboBoxNote->setCurrentIndex(11);
	else comboBoxNote->setCurrentIndex(0);
	comboBoxNote->blockSignals(false);
	
	spinBoxOctave->setValue(signal()->elementName().section(QRegExp("_(Do|Dod|Re|Mib|Mi|Fa|Fad|Sol|Lab|La|Sib|Si)"), 1, -1).replace(QString("y"), QString("-1")).replace(QString("z"), QString("-2")).toInt());
	//spinBoxOctave->setValue(QString(signal()->elementName().at(signal()->elementName().size() - 1)).replace(QString("y"), QString("-1")).replace(QString("z"), QString("-2")).toInt());
	}
else
	{
	QMessageBox::critical(this, tr("MIDI signal editor error"), tr("<h3>MIDI note signal editor error</h3><p>An error is occured while the reading of the signal \"%1\": no MIDI note specifications was found.</p>").arg(signal()->elementName()));
	}
}

QString MecMidiNoteSignalEditor::signalName() const
{
QString tempName(MecMidiSignalEditor::signalName());
tempName += "_";

switch (comboBoxNote->currentIndex())
	{
	case 0 : tempName.append("Do"); break;
	case 1 : tempName.append("Dod"); break;
	case 2 : tempName.append("Re"); break;
	case 3 : tempName.append("Mib"); break;
	case 4 : tempName.append("Mi"); break;
	case 5 : tempName.append("Fa"); break;
	case 6 : tempName.append("Fad"); break;
	case 7 : tempName.append("Sol"); break;
	case 8 : tempName.append("Lab"); break;
	case 9 : tempName.append("La"); break;
	case 10 : tempName.append("Sib"); break;
	case 11 : tempName.append("Si"); break;
	default : tempName.append("Do"); break;
	}

tempName += QString::number(spinBoxOctave->value()).replace(QString("-1"), QString("y")).replace(QString("-2"), QString("z"));//.replace(QString("-1"), QString("y")).replace(QString("-2"), QString("z"));
return tempName;
}

