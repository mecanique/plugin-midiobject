/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECMIDINOTESIGNALEDITOR_H__
#define __MECMIDINOTESIGNALEDITOR_H__

#include "MecMidiSignalEditor.h"

/**
\brief	Classe d'édition de signal MIDI correspondant à une note.

Cette classe fournit les précisions nécessaires à l'édition d'un signal MIDI de type « Note », à savoir « Note on », « Note off » et « Polyphonic aftertouch ».
*/
class MecMidiNoteSignalEditor : public MecMidiSignalEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecMidiNoteSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecMidiNoteSignalEditor();

	/**
	\brief	Retourne l'expression régulière correspondant à un nom de signal MIDI de type « Note ».
	*/
	static QRegExp midiName();

public slots:
	/**
	\brief	Est déclenché lorsque le nom de l'élément a changé.

	Cette procédure assigne les propriétés du signal MIDI d'après son nom.
	*/
	virtual void nameElementChanged(MecAbstractElement *Element);

signals:


protected:
	///Label de note.
	QLabel *labelNote;
	///Liste déroulante des notes.
	QComboBox *comboBoxNote;
	///Label d'octave.
	QLabel *labelOctave;
	///Sélecteur d'octave.
	QSpinBox *spinBoxOctave;

	/**
	\brief	Retourne le nom du signal tel qu'il doît être pour correspondre aux données de l'éditeur.
	*/
	virtual QString signalName() const;

};

#endif /* __MECMIDINOTESIGNALEDITOR_H__ */

