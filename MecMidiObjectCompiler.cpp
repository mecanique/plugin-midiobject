/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecMidiObjectCompiler.h"

MecMidiObjectCompiler::MecMidiObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler) : MecObjectCompiler(Object, MainCompiler)
{
}

MecMidiObjectCompiler::~MecMidiObjectCompiler()
{
}
	
QList<QResource*> MecMidiObjectCompiler::resources()
{
QList<QResource*> tempList;
tempList.append(new QResource(":/share/icons/types/Midi.png"));
tempList.append(new QResource(":/src/midi/MidiConnection.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/include/ksmedia.h"));
tempList.append(new QResource(":/src/midi/rtmidi/include/ks.h"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/qmidiin.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/midiout.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/qmidiin.dsp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/cmidiin.dsp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/midiprobe.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/sysextest.dsp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/midiout.dsp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/sysextest.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/RtMidi.dsw"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/Makefile.in"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/midiprobe.dsp"));
tempList.append(new QResource(":/src/midi/rtmidi/tests/cmidiin.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/configure"));
tempList.append(new QResource(":/src/midi/rtmidi/msw/rtmidilib.sln"));
tempList.append(new QResource(":/src/midi/rtmidi/msw/rtmidilib.vcproj"));
tempList.append(new QResource(":/src/midi/rtmidi/msw/readme"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/images/mcgill.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/images/ccrma.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/hierarchy.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutWinMM-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structRtMidiIn_1_1MidiQueue-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/doxygen.css"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/functions_eval.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInApi.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInCore.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/tab_r.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInDummy.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInAlsa-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInAlsa.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structRtMidiIn_1_1MidiQueue.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/files.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInApi-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structMidiInApi_1_1MidiMessage.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structMidiInApi_1_1MidiMessage-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInWinMM-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutApi-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInWinKS-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutCore-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structMidiInApi_1_1RtMidiInData-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiOut.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInWinMM.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/doxygen.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structRtMidiIn_1_1RtMidiInData.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInCore.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutWinKS.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInWinKS.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutJack.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiIn-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/tab_l.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiOut-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutWinKS-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/functions.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidi.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutWinKS.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiIn.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInApi.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/functions_func.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutDummy.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidi-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInJack.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/RtMidi_8h_source.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiOut.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutApi.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidi.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiOut.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classes.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiIn.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/index.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutDummy-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutAlsa-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutWinMM.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/annotated.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structMidiInApi_1_1MidiQueue-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutJack-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutJack.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/tab_b.gif"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structMidiInApi_1_1RtMidiInData.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutDummy.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInDummy-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInCore-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/functions_type.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutCore.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInWinKS.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInJack.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutApi.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structMidiInApi_1_1MidiQueue.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structRtMidiIn_1_1MidiMessage.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutCore.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/RtError_8h-source.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInJack-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structRtMidiIn_1_1MidiMessage-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/RtError_8h_source.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInWinMM.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInDummy.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidi.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/RtMidi_8h-source.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutAlsa.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/functions_enum.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiInAlsa.png"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/RtMidi_8h.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtMidiIn.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutAlsa.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classMidiOutWinMM.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/tabs.css"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtError-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/structRtMidiIn_1_1RtMidiInData-members.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/html/classRtError.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/doxygen/Doxyfile"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/doxygen/header.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/doxygen/footer.html"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/doxygen/tutorial.txt"));
tempList.append(new QResource(":/src/midi/rtmidi/doc/release.txt"));
tempList.append(new QResource(":/src/midi/rtmidi/configure.ac"));
tempList.append(new QResource(":/src/midi/rtmidi/rtmidi-config.in"));
tempList.append(new QResource(":/src/midi/rtmidi/Makefile.in"));
tempList.append(new QResource(":/src/midi/rtmidi/RtMidi.cpp"));
tempList.append(new QResource(":/src/midi/rtmidi/config/install.sh"));
tempList.append(new QResource(":/src/midi/rtmidi/config/config.sub"));
tempList.append(new QResource(":/src/midi/rtmidi/config/config.guess"));
tempList.append(new QResource(":/src/midi/rtmidi/RtError.h"));
tempList.append(new QResource(":/src/midi/rtmidi/RtMidi.h"));
tempList.append(new QResource(":/src/midi/rtmidi/readme"));
tempList.append(new QResource(":/src/midi/MidiObject.h"));
tempList.append(new QResource(":/src/midi/MidiConnection.h"));
tempList.append(new QResource(":/src/midi/MidiObject.cpp"));

tempList.append(new QResource(":/share/translations/Midi.fr.qm"));
return tempList;
}

QString MecMidiObjectCompiler::projectInstructions()
{
QString tempString;
tempString += "HEADERS += ./midi/MidiObject.h ./midi/MidiConnection.h ./midi/rtmidi/RtError.h ./midi/rtmidi/RtMidi.h\n";
tempString += "SOURCES += ./midi/MidiObject.cpp ./midi/MidiConnection.cpp ./midi/rtmidi/RtMidi.cpp\n";
tempString += "unix:!macx {\n\
DEFINES += __LINUX_ALSA__ \n\
LIBS += -lasound -lpthread \n\
}\n\
macx {\n\
DEFINES += __MACOSX_CORE__ \n\
LIBS += -framework CoreMIDI -framework CoreAudio -framework \n\
}\n";
return tempString;
}
	
QString MecMidiObjectCompiler::header()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "#ifndef __" + object()->elementName().toUpper() + "_H__\n#define __" + object()->elementName().toUpper() + "_H__\n\n#include \"midi/MidiObject.h\"\n";

for (int i=0 ; i < recConcreteSubCompilers().size() ; i++)
	{
	tempString += recConcreteSubCompilers().at(i)->preprocessorInstructions();
	}

tempString += "\nclass " + object()->elementName() + " : public MidiObject\n{\nQ_OBJECT\n\tpublic:\n";
tempString += object()->elementName() + "(Project* const Project);\n~" + object()->elementName() + "();\n\n\tpublic:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Variable) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}


tempString += "\tpublic slots:\n";
for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function and concreteSubCompilers().at(i)->element()->elementName() != "sendMessage" and concreteSubCompilers().at(i)->element()->elementName() != "receivesMessage") tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tsignals:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal and concreteSubCompilers().at(i)->element()->elementName() != "messageReceived") tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tprivate slots:\nvoid receivesMessage(QVector<unsigned char> message);\n";

tempString += "};\n\n\n#endif /* __" + object()->elementName().toUpper() + "_H__ */\n\n";

return tempString;
}

QString MecMidiObjectCompiler::source()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "\n#include \"" + object()->elementName() + ".h\"\n\n";

tempString += object()->elementName() + "::" + object()->elementName() + "(Project* const Project) : MidiObject(\"" + object()->elementName() + "\", Project)\n{\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Variable) tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

tempString += "}\n\n" + object()->elementName() + "::~" + object()->elementName() + "()\n{\n}\n\n";

//Implémentation du sélecteur de signaux MIDI.
tempString += QString("void ") + object()->elementName() + "::receivesMessage(QVector<unsigned char> message)\n{\n";

for (int c=1 ; c <= 16 ; c++)
{
	for (int i=0 ; i < concreteSubCompilers().size() ; i++)
		{
		if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal and MecMidiSignalEditor::midiName().exactMatch(concreteSubCompilers().at(i)->element()->elementName()) and QRegExp("^C" + QString::number(c) + "_.+").exactMatch(concreteSubCompilers().at(i)->element()->elementName()))
			{
			QString event(concreteSubCompilers().at(i)->element()->elementName().section(QRegExp("(^C)|_"), 2, 2));
			if (event == "NoteOff")
				{
				if (QRegExp("^C" + QString::number(c) + "_NoteOff$").exactMatch(concreteSubCompilers().at(i)->element()->elementName()))
					{
					tempString += "if (message.size() == 3 and ((message.at(0) == " + QString::number(127 + c) + ") or (message.at(0) == " + QString::number(143 + c) + " and message.at(2) == 0))) emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1), (int)message.at(2));\n";
					}
				else
					{
					tempString += "if (message.size() == 3 and ((message.at(0) == " + QString::number(127 + c) + " and message.at(1) == " + QString::number(noteNameToNumber(concreteSubCompilers().at(i)->element()->elementName().section(QRegExp("(^C)|_"), 3, 3))) + ") or (message.at(0) == " + QString::number(143 + c) + " and message.at(1) == " + QString::number(noteNameToNumber(concreteSubCompilers().at(i)->element()->elementName().section(QRegExp("(^C)|_"), 3, 3))) + " and message.at(2) == 0))) emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(2));\n";
					}
				}
			else if (event == "NoteOn")
				{
				if (QRegExp("^C" + QString::number(c) + "_NoteOn$").exactMatch(concreteSubCompilers().at(i)->element()->elementName()))
					{
					tempString += "if (message.size() == 3 and message.at(0) == " + QString::number(143 + c) + " and message.at(2) != 0) emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1), (int)message.at(2));\n";
					}
				else
					{
					tempString += "if (message.size() == 3 and message.at(0) == " + QString::number(143 + c) + " and message.at(1) == " + QString::number(noteNameToNumber(concreteSubCompilers().at(i)->element()->elementName().section(QRegExp("(^C)|_"), 3, 3))) + " and message.at(2) != 0) emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(2));\n";
					}
				}
			else if (event == "PolyAfter")
				{
				if (QRegExp("^C" + QString::number(c) + "_PolyAfter$").exactMatch(concreteSubCompilers().at(i)->element()->elementName()))
					{
					tempString += "if (message.size() == 3 and message.at(0) == " + QString::number(159 + c) + ") emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1), (int)message.at(2));\n";
					}
				else
					{
					tempString += "if (message.size() == 3 and message.at(0) == " + QString::number(159 + c) + " and message.at(1) == " + QString::number(noteNameToNumber(concreteSubCompilers().at(i)->element()->elementName().section(QRegExp("(^C)|_"), 3, 3))) + ") emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(2));\n";
					}
				}
			else if (event == "Control")
				{
				tempString += "if (message.size() == 3 and message.at(0) == " + QString::number(175 + c) + ") emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1), (int)message.at(2));\n";
				}
			else if (event == "ProgChange")
				{
				tempString += "if (message.size() == 2 and message.at(0) == " + QString::number(191 + c) + ") emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1));\n";
				}
			else if (event == "ChannelAfter")
				{
				tempString += "if (message.size() == 2 and message.at(0) == " + QString::number(207 + c) + ") emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1));\n";
				}
			else if (event == "PitchWheel")
				{
				tempString += "if (message.size() == 2 and message.at(0) == " + QString::number(223 + c) + ") emit " + concreteSubCompilers().at(i)->element()->elementName() + "((int)message.at(1));\n";
				}
			}
			/*else if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal and concreteSubCompilers().at(i)->element()->elementName() != "messageReceived")
			{
			tempString += concreteSubCompilers().at(i)->sourceInstructions();
			}*/
		}
}

tempString += "\n}\n\n";
//Fin de l'implémentation du sélecteur de signaux MIDI.

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function and concreteSubCompilers().at(i)->element()->elementName() != "sendMessage" and concreteSubCompilers().at(i)->element()->elementName() != "receivesMessage") tempString += concreteSubCompilers().at(i)->sourceInstructions();
	}

return tempString;
}

int MecMidiObjectCompiler::noteNameToNumber(QString Name)
{
int number = -1;
if (QRegExp("^(Do|Dod|Re|Mib|Mi|Fa|Fad|Sol|Lab|La|Sib|Si)(z|y|[0-8])$").exactMatch(Name))
	{
	QString note(QString(Name).remove(QRegExp("(z|y|[0-8])$")));
	int octave = QString(Name).remove(QRegExp("(Do|Dod|Re|Mib|Mi|Fa|Fad|Sol|Lab|La|Sib|Si)")).replace(QString("y"), QString("-1")).replace(QString("z"), QString("-2")).toInt();
	
	/*
	Chaque octave est réparti sur 12 notes, et le Do de l'octave -2 est noté 0 en MIDI.
	*/
	number = (octave + 2) * 12;
	
	int numNote = 0;
	if (note == "Do") numNote = 0;
	else if (note == "Dod") numNote = 1;
	else if (note == "Re") numNote = 2;
	else if (note == "Mib") numNote = 3;
	else if (note == "Mi") numNote = 4;
	else if (note == "Fa") numNote = 5;
	else if (note == "Fad") numNote = 6;
	else if (note == "Sol") numNote = 7;
	else if (note == "Lab") numNote = 8;
	else if (note == "La") numNote = 9;
	else if (note == "Sib") numNote = 10;
	else if (note == "Si") numNote = 11;
	
	number += numNote;
	}
return number;
}


