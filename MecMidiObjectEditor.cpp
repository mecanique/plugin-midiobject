/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecMidiObjectEditor.h"

MecMidiObjectEditor::MecMidiObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecObjectEditor(Object, MainEditor, Parent, F)
{
}

MecMidiObjectEditor::~MecMidiObjectEditor()
{
}
	
bool MecMidiObjectEditor::canRemoveChild(MecAbstractElement* const Element) const
{
if (Element->elementName() == "sendMessage" or Element->elementName() == "messageReceived") return false;
else return true;
}

void MecMidiObjectEditor::addSignal()
{
QMessageBox tempBox(QMessageBox::NoIcon,
			tr("Select signal type"),
			tr("Select the signal type:"),
			QMessageBox::NoButton,
			this,
			Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
tempBox.addButton(new QPushButton(QIcon(":/share/icons/types/void.png"), "void", &tempBox), QMessageBox::NoRole);
tempBox.addButton(new QPushButton(QIcon(":/share/icons/types/Midi.png"), "MIDI", &tempBox), QMessageBox::YesRole);

int result = tempBox.exec();
if (result)
	{
	QDir tempDir(":/share/base/signal/Midi");
	QStringList returnList;
	returnList = tempDir.entryList(QDir::Files, QDir::Name);
	returnList.replaceInStrings(".mec", "");
	
	bool valid = false;
	QString fileName = QInputDialog::getItem(this, tr("Select MIDI event"), tr("Select the MIDI event:"), returnList, 0, false, &valid);
	if (!valid) return;

	MecAbstractElement *tempElement = mainEditor()->read(":/share/base/signal/Midi/" + fileName + ".mec");
	
	QStringList childListNames;
	for (int i=0 ; i < object()->childElements().size() ; i++) childListNames << object()->childElements().at(i)->elementName();
	
	//On teste la disponibilité MIDI du nom…
	QString tempName = tempElement->elementName();
	bool Ok = !childListNames.contains(tempName);
	while (!Ok and MecMidiNoteSignalEditor::midiName().exactMatch(tempName))//Pour les évènements de note…
		{
		//tempName.remove(QRegExp("^_+"));
		int channel = tempName.section(QRegExp("C|_"), 1, 1).toInt();
		QString event = tempName.section(QRegExp("C|_"), 2, 2);
		QString note = tempName.section(QRegExp("C|_"), 3, 3).remove(QRegExp("(z|y|[0-8])$"));
		int octave = tempName.section(QRegExp("_(Do|Dod|Re|Mib|Mi|Fa|Fad|Sol|Lab|La|Sib|Si)"), 1, -1).replace(QString("y"), QString("-1")).replace(QString("z"), QString("-2")).toInt();//QString(tempName.at(tempName.size() - 1)).replace(QString("y"), QString("-1")).replace(QString("z"), QString("-2")).toInt();
		
		if (note == "Do") note = "Dod";
		else if (note == "Dod") note = "Re";
		else if (note == "Re") note = "Mib";
		else if (note == "Mib") note = "Mi";
		else if (note == "Mi") note = "Fa";
		else if (note == "Fa") note = "Fad";
		else if (note == "Fad") note = "Sol";
		else if (note == "Sol") note = "Lab";
		else if (note == "Lab") note = "La";
		else if (note == "La") note = "Sib";
		else if (note == "Sib") note = "Si";
		else if (note == "Si")
			{
			note = "Do"; 
			if (octave < 5) octave++;
			else
				{
				octave = -2;
				if (event == "NoteOff") event = "NoteOn";
				else if (event == "NoteOn") event = "PolyAfter";
				else //càd event == "PolyAfter"
					{
					event = "NoteOff";
					
					if (channel < 16) channel++;
					else //On est alors dans l'impasse et tout a été essayé… Il n'y a donc plus de signal disponible.
						{
						QMessageBox::critical(this, tr("No more MIDI note message available..."), tr("<h3>No more MIDI note message available</h3><p>All MIDI note messages (Note on, Note off and Polyphonic aftertouch) are defined, it is not possible to append a new signal.</p>"));
						delete tempElement;
						return;
						}
					}
				}
			}
		
		tempName = QString("C") + QString::number(channel) + QString("_") + event + QString("_") + note + QString::number(octave).replace(QString("-1"), QString("y")).replace(QString("-2"), QString("z"));
		Ok = !childListNames.contains(tempName);
		}
	while (!Ok and MecMidiProgramChangeSignalEditor::midiName().exactMatch(tempName))//Pour les évènements de changement de programme…
		{
		int channel = tempName.section(QRegExp("C|_"), 1, 1).toInt();
		int program = tempName.section(QRegExp("C|_"), 3, 3).toInt();
		
		if (program < 127) program++;
		else
			{
			program = 0;
			
			if (channel < 16) channel++;
			else //On est alors dans l'impasse et tout a été essayé… Il n'y a donc plus de signal disponible.
				{
				QMessageBox::critical(this, tr("No more MIDI program change message available..."), tr("<h3>No more MIDI program change message available</h3><p>All MIDI program change messages are defined, it is not possible to append a new signal.</p>"));
				delete tempElement;
				return;
				}
			}
		
		tempName = QString("C") + QString::number(channel) + QString("_ProgChange_") + QString::number(program);
		Ok = !childListNames.contains(tempName);
		}
	while (!Ok and MecMidiSignalEditor::midiName().exactMatch(tempName))//Et pour les autres…
		{
		int channel = tempName.section(QRegExp("C|_"), 1, 1).toInt();
		QString id = tempName.section(QRegExp("C|_"), 2, -1);
		
		if (channel < 16) channel++;
		else //On est alors dans l'impasse et tout a été essayé… Il n'y a donc plus de signal disponible.
			{
			QMessageBox::critical(this, tr("No more MIDI message available..."), tr("<h3>No more MIDI message of this type is available</h3><p>All MIDI messages of this type are defined, it is not possible to append a new signal.</p>"));
			delete tempElement;
			return;
			}
		
		tempName = QString("C") + QString::number(channel) + QString("_") + id;
		Ok = !childListNames.contains(tempName);
		}
	
	tempElement->setElementName(tempName);
	tempElement->setParentElement(object());
	mainEditor()->addEditStep(tr("Add the signal “%1”").arg(tempElement->elementName()));
	}
else
	{
	MecAbstractElement *tempElement = mainEditor()->baseElement(MecAbstractElement::Signal, "void");
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the signal “%1”").arg(tempElement->elementName()));
	}
}
	
MecAbstractElementEditor* MecMidiObjectEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() == MecAbstractElement::Signal and MecMidiNoteSignalEditor::midiName().exactMatch(Element->elementName()))
	{
	MecMidiNoteSignalEditor *tempEditor = new MecMidiNoteSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
	tempEditor->show();
	return tempEditor;
	}
else if (Element->elementRole() == MecAbstractElement::Signal and MecMidiProgramChangeSignalEditor::midiName().exactMatch(Element->elementName()))
	{
	MecMidiProgramChangeSignalEditor *tempEditor = new MecMidiProgramChangeSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
	tempEditor->show();
	return tempEditor;
	}
else if (Element->elementRole() == MecAbstractElement::Signal and MecMidiSignalEditor::midiName().exactMatch(Element->elementName()))
	{
	MecMidiSignalEditor *tempEditor = new MecMidiSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
	tempEditor->show();
	return tempEditor;
	}
else if (Element->elementName() == QString("sendMessage") and Element->elementRole() == MecAbstractElement::Function)
	{
	MecElementEditor *tempEditor = new MecMidiSendMessageFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	tempEditor->show();
	return tempEditor;
	}
else if (Element->elementName() == QString("messageReceived") and Element->elementRole() == MecAbstractElement::Signal)
	{
	MecElementEditor *tempEditor = new MecMidiMessageReceivedSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	tempEditor->show();
	return tempEditor;
	}
else 
	{
	return MecObjectEditor::newSubEditor(Element);
	}
}

