/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECMIDIOBJECTEDITOR_H__
#define __MECMIDIOBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include "MecMidiSignalEditor.h"
#include "MecMidiNoteSignalEditor.h"
#include "MecMidiProgramChangeSignalEditor.h"
#include "MecMidiMessageReceivedSignalEditor.h"
#include "MecMidiSendMessageFunctionEditor.h"

/**
\brief	Classe virtuelle d'édition d'objet MIDI.

Cette classe fournit les précisions nécessaires à l'édition d'un objet de type MIDI.
*/
class MecMidiObjectEditor : public MecObjectEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecMidiObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecMidiObjectEditor();

	///Indique si un élément enfant peut être supprimé, cela dépend ici de l'élément.
	bool canRemoveChild(MecAbstractElement* const Element) const;

public slots:
	/**
	\brief	Ajoute un signal à l'élément.

	\see	addChild(MecAbstractElement::ElementRole ElementRole)
	*/
	void addSignal();

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

signals:


private:


};

#endif /* __MECMIDIOBJECTEDITOR_H__ */

