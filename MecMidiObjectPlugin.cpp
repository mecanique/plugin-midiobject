/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecMidiObjectPlugin.h"

MecMidiObjectPlugin::MecMidiObjectPlugin() : MecPlugin(QString("midiobject"), __MIDIOBJECT_VERSION__, MecAbstractElement::Object, QString("Midi"), QString("Midi object"))
{
}

MecMidiObjectPlugin::~MecMidiObjectPlugin()
{
}

QString MecMidiObjectPlugin::description() const
{
return QString(tr("Manage the Musical Instrument Digital Interface protocol."));
}

QString MecMidiObjectPlugin::copyright() const
{
return QString("Copyright © 2013 – 2015 Quentin VIGNAUD");
}

QString MecMidiObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecMidiObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecMidiObjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecMidiObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecMidiObjectEditor *tempEditor = new MecMidiObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecMidiObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecMidiObjectCompiler *tempCompiler = new MecMidiObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}


