/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecMidiProgramChangeSignalEditor.h"

MecMidiProgramChangeSignalEditor::MecMidiProgramChangeSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecMidiSignalEditor(Signal, MainEditor, Parent, F)
{
labelProgram = new QLabel(tr("Program:"), widgetProperties);
spinBoxProgram = new QSpinBox(widgetProperties);
	spinBoxProgram->setRange(0, 127);
	spinBoxProgram->setValue(0);
	connect(spinBoxProgram, SIGNAL(editingFinished()), SLOT(updateName()));

addGeneralWidget(labelProgram);
addGeneralWidget(spinBoxProgram);

MecMidiProgramChangeSignalEditor::nameElementChanged(signal());
}

MecMidiProgramChangeSignalEditor::~MecMidiProgramChangeSignalEditor()
{
//Rien de particulier.
}
	
QRegExp MecMidiProgramChangeSignalEditor::midiName()
{
return QRegExp("^C([1-9]|1[0-6])_ProgChange_([0-9]|[0-9][0-9]|1[01][0-9]|12[0-7])$");
}
	
void MecMidiProgramChangeSignalEditor::nameElementChanged(MecAbstractElement *Element)
{
MecMidiSignalEditor::nameElementChanged(Element);

if (midiName().exactMatch(signal()->elementName()))
	{	
	spinBoxProgram->setValue(signal()->elementName().section(QRegExp("(^C)|_"), 3, 3).toInt());
	}
else
	{
	QMessageBox::critical(this, tr("MIDI signal editor error"), tr("<h3>MIDI note signal editor error</h3><p>An error is occured while the reading of the signal \"%1\": no MIDI program change specifications was found.</p>").arg(signal()->elementName()));
	}
}

QString MecMidiProgramChangeSignalEditor::signalName() const
{
QString tempName(MecMidiSignalEditor::signalName());
tempName += "_";

tempName += QString::number(spinBoxProgram->value());
return tempName;
}


