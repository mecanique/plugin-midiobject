/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecMidiSignalEditor.h"

MecMidiSignalEditor::MecMidiSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecSignalEditor(Signal, MainEditor, Parent, F)
{
lineEditName->setEnabled(false);

labelChannel = new QLabel(tr("Channel:"), widgetProperties);
spinBoxChannel = new QSpinBox(widgetProperties);
	spinBoxChannel->setRange(1, 16);
	spinBoxChannel->setValue(1);
	connect(spinBoxChannel, SIGNAL(editingFinished()), SLOT(updateName()));

labelEvent = new QLabel(tr("Event:"), widgetProperties);
comboBoxEvent = new QComboBox(widgetProperties);
	comboBoxEvent->addItems(QStringList() << tr("Note off") << tr("Note on") << tr("Polyphonic aftertouch") << tr("Control") << tr("Program change") << tr("Channel aftertouch") << tr("Pitch wheel"));
	comboBoxEvent->setEnabled(false);
	//connect(comboBoxEvent, SIGNAL(currentIndexChanged(int)), SLOT(updateName()));

addGeneralWidget(labelChannel);
addGeneralWidget(spinBoxChannel);
addGeneralWidget(labelEvent);
addGeneralWidget(comboBoxEvent);

widgetEditorsVariables->setEnabled(false);

MecMidiSignalEditor::nameElementChanged(signal());
}

MecMidiSignalEditor::~MecMidiSignalEditor()
{
//Rien de particulier.
}
	
bool MecMidiSignalEditor::canAddVariable() const
{
return false;
}

bool MecMidiSignalEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return false;
}
	
QRegExp MecMidiSignalEditor::midiName()
{
return QRegExp("^C([1-9]|1[0-6])_(NoteOff|NoteOn|PolyAfter|Control|ProgChange|ChannelAfter|PitchWheel)(_((Do|Dod|Re|Mib|Mi|Fa|Fad|Sol|Lab|La|Sib|Si)(z|y|[0-8])|([0-9]|[0-9][0-9]|1[01][0-9]|12[0-7])))?$");
}
	
void MecMidiSignalEditor::nameElementChanged(MecAbstractElement *Element)
{
MecSignalEditor::nameElementChanged(Element);

if (midiName().exactMatch(signal()->elementName()))
	{
	spinBoxChannel->setValue(signal()->elementName().section(QRegExp("(^C)|_"), 1, 1).toInt());
	
	comboBoxEvent->blockSignals(true);
	QString event(signal()->elementName().section(QRegExp("(^C)|_"), 2, 2));
	if (event == "NoteOff") comboBoxEvent->setCurrentIndex(0);
	else if (event == "NoteOn") comboBoxEvent->setCurrentIndex(1);
	else if (event == "PolyAfter") comboBoxEvent->setCurrentIndex(2);
	else if (event == "Control") comboBoxEvent->setCurrentIndex(3);
	else if (event == "ProgChange") comboBoxEvent->setCurrentIndex(4);
	else if (event == "ChannelAfter") comboBoxEvent->setCurrentIndex(5);
	else if (event == "PitchWheel") comboBoxEvent->setCurrentIndex(6);
	else comboBoxEvent->setCurrentIndex(0);
	comboBoxEvent->blockSignals(false);
	}
else
	{
	QMessageBox::critical(this, tr("MIDI signal editor error"), tr("<h3>MIDI signal editor error</h3><p>An error is occured while the reading of the signal \"%1\" no MIDI specifications was found.</p>").arg(signal()->elementName()));
	}
}

QString MecMidiSignalEditor::signalName() const
{
QString tempName("C");
tempName += QString::number(spinBoxChannel->value()) + "_";

switch (comboBoxEvent->currentIndex())
	{
		case 0 :
			tempName.append("NoteOff");
			break;
		case 1 :
			tempName.append("NoteOn");
			break;
		case 2 :
			tempName.append("PolyAfter");
			break;
		case 3 :
			tempName.append("Control");
			break;
		case 4 :
			tempName.append("ProgChange");
			break;
		case 5 :
			tempName.append("ChannelAfter");
			break;
		case 6 :
			tempName.append("PitchWheel");
			break;
		default:
			tempName.append("NoteOff");
			break;
	}
return tempName;
}
	
void MecMidiSignalEditor::updateName()
{
QString oldName = element()->elementName();
if (!signal()->setElementName(signalName()))
	{
	nameElementChanged(signal());
	mainEditor()->addEditStep(tr("Change name of “%1” to “%2”").arg(oldName).arg(signal()->elementName()));
	}
}


