/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECMIDISIGNALEDITOR_H__
#define __MECMIDISIGNALEDITOR_H__

#include <MecSignalEditor.h>

/**
\brief	Classe d'édition de signal MIDI.

Cette classe fournit les précisions nécessaires à l'édition d'un signal MIDI.
*/
class MecMidiSignalEditor : public MecSignalEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecMidiSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecMidiSignalEditor();

	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e false.
	bool canRemoveChild(MecAbstractElement* const Element) const;

	/**
	\brief	Retourne l'expression régulière correspondant à un nom de signal MIDI.
	*/
	static QRegExp midiName();

public slots:
	/**
	\brief	Est déclenché lorsque le nom de l'élément a changé.

	Cette procédure assigne les propriétés du signal MIDI d'après son nom.
	*/
	virtual void nameElementChanged(MecAbstractElement *Element);

signals:


protected:
	///Label de canal.
	QLabel *labelChannel;
	///Sélecteur de canal.
	QSpinBox *spinBoxChannel;

	///Label d'évènement.
	QLabel *labelEvent;
	///Sélecteur d'évènement.
	QComboBox *comboBoxEvent;

	/**
	\brief	Retourne le nom du signal tel qu'il doît être pour correspondre aux données de l'éditeur.
	*/
	virtual QString signalName() const;

protected slots:
	/**
	\brief	Met à jour le nom du signal.

	Si le nouveau nom n'est pas disponible, cette procédure rappelle « nameElementChanged() ».
	*/
	void updateName();


};

#endif /* __MECMIDISIGNALEDITOR_H__ */

