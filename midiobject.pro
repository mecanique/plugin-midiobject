######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = midiobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/midiobject.fr.ts \
               translations/midiobject.de.ts

OTHER_FILES += metadata.json

HEADERS +=	    MecMidiObjectEditor.h \
                MecMidiSignalEditor.h \
                MecMidiNoteSignalEditor.h \
                MecMidiProgramChangeSignalEditor.h \
                MecMidiMessageReceivedSignalEditor.h \
                MecMidiSendMessageFunctionEditor.h \
                MecMidiObjectCompiler.h \
                MecMidiObjectPlugin.h
              	


SOURCES +=	    MecMidiObjectEditor.cpp \
                MecMidiSignalEditor.cpp \
                MecMidiNoteSignalEditor.cpp \
                MecMidiProgramChangeSignalEditor.cpp \
                MecMidiMessageReceivedSignalEditor.cpp \
                MecMidiSendMessageFunctionEditor.cpp \
                MecMidiObjectCompiler.cpp \
                MecMidiObjectPlugin.cpp
                


