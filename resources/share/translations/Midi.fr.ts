<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MidiObject</name>
    <message>
        <source>MIDI system loading...</source>
        <translation>Chargement du système MIDI…</translation>
    </message>
    <message>
        <source>MIDI system is running.</source>
        <translation>Système MIDI actif.</translation>
    </message>
    <message>
        <source>MIDI system failed.</source>
        <translation>Système MIDI en échec.</translation>
    </message>
</context>
</TS>
