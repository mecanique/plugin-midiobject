/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MidiConnection.h"

MidiConnection::MidiConnection(QString Name, QObject *Object) : QThread(Object)
{
qRegisterMetaType<QVector<unsigned char> >("QVector<unsigned char>");

midiIn = new RtMidiIn(RtMidi::UNSPECIFIED, "Mecanique");
	midiIn->openPort(0, Name.toStdString());

midiOut = new RtMidiOut(RtMidi::UNSPECIFIED, "Mecanique");
	midiOut->openPort(0, Name.toStdString());
}

MidiConnection::~MidiConnection()
{
delete midiIn;
delete midiOut;
}

void MidiConnection::sendMessage(QVector<unsigned char> message)
{
std::vector<unsigned char> tempVector = message.toStdVector();
midiOut->sendMessage(&tempVector);
}
	
void MidiConnection::run()
{
while (1)
	{
	std::vector<unsigned char> tempVector;
	midiIn->getMessage(&tempVector);
	
	if (tempVector.empty())
		{
		QThread::msleep(1);
		}
	else
		{
		emit messageReceived(QVector<unsigned char>::fromStdVector(tempVector));
		}
	}
}


