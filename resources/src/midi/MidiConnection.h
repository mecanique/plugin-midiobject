/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MIDICONNECTION_H__
#define __MIDICONNECTION_H__

#include <QtCore>
#include "rtmidi/RtMidi.h"

Q_DECLARE_METATYPE(QVector<unsigned char>)

/**
\brief	Classe de représentation de connexion MIDI.
*/
class MidiConnection : public QThread
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	\param	Name	Nom de la connexion.
	\param	Object	Objet parent.
	
	Ouvre la connexion.
	*/
	MidiConnection(QString Name, QObject *Object=0);
	/**
	\brief	Destructeur.
	
	Ferme la connexion.
	*/
	~MidiConnection();
	
	public slots:
	/**
	\brief	Envoi le message MIDI.
	
	Renseignement sur les message MIDI : http://www.midi.org/techspecs/midimessages.php
	*/
	void sendMessage(QVector<unsigned char> message);
	
	signals:
	/**
	\brief	Est émis lors de la réception d'un message MIDI.
	
	Renseignement sur les message MIDI : http://www.midi.org/techspecs/midimessages.php
	*/
	void messageReceived(QVector<unsigned char> message);
	
	
	private:
	///Exécution de la connexion.
	void run();
	
	///Connexion entrante.
	RtMidiIn *midiIn;
	///Connexion sortante.
	RtMidiOut *midiOut;
	
};

#endif /* __MIDICONNECTION_H__ */

