/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MidiObject.h"

MidiObject::MidiObject(QString Name, Project* const Project) : Object(Name, "Midi", Project)
{
m_connection = new MidiConnection(Name, this);
	connect(m_connection, SIGNAL(started()), SLOT(connectionStarted()));
	connect(m_connection, SIGNAL(finished()), SLOT(connectionFinished()));
	connect(m_connection, SIGNAL(messageReceived(QVector<unsigned char>)), SLOT(messageProcessing(QVector<unsigned char>)));
m_connection->start();

changeInfos(tr("MIDI system loading..."));
}

MidiObject::~MidiObject()
{
m_connection->quit();
if (!m_connection->wait(1000)) m_connection->terminate();
}
	
void MidiObject::more()
{

}
	
QVariant MidiObject::settings()
{
return QVariant();
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
void MidiObject::setSettings(QVariant Settings)
{
return;
}
#pragma GCC diagnostic pop
	
void MidiObject::sendMessage(int first, int second, int third)
{
QVector<unsigned char> tempVector;
tempVector.append((unsigned char)first);
if (second >= 0)
	{
	tempVector.append((unsigned char)second);
	if (third >= 0)
		{
		tempVector.append((unsigned char)third);
		}
	}
m_connection->sendMessage(tempVector);
}

void MidiObject::sendMessage(QVector<unsigned char> message)
{
m_connection->sendMessage(message);
}

void MidiObject::messageProcessing(QVector<unsigned char> message)
{
int first = message.at(0);
int second = -1;
int third = -1;
if (message.size() > 1)
	{
	second = message.at(1);
	if (message.size() > 2)
		{
		third = message.at(2);
		}
	}

QString tempText("MIDI message received:");
for (int i=0 ; i < message.size() ; i++) tempText += " 0x" + QString::number(message.at(i), 16);
project()->log()->write(name(), tempText);

emit messageReceived(first, second, third);

receivesMessage(message);
}

void MidiObject::connectionStarted()
{
changeStatus(Object::Operational);
changeInfos(tr("MIDI system is running."));
project()->log()->write(name(), "Connection started.");
}

void MidiObject::connectionFinished()
{
changeStatus(Object::Error);
changeInfos(tr("MIDI system failed."));
project()->log()->write(name(), "Connection finished.");
}


