/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MIDIOBJECT_H__
#define __MIDIOBJECT_H__

#include "../mecanique/Object.h"
#include "MidiConnection.h"

/**
\brief	Classe de représentation d'objet MIDI.
*/
class MidiObject : public Object
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Name	Nom de l'objet.
	\param	Projet	Projet d'appartenance.
	*/
	MidiObject(QString Name, Project* const Project);
	///Destructeur.
	virtual ~MidiObject();
	
	///Ne fait rien.
	virtual void more();
	
	///Retourne un QVariant vide.
	virtual QVariant settings();
	///Ne fait rien.
	virtual void setSettings(QVariant Settings);
	
	public slots:
	/**
	\brief	Envoie un message MIDI avec les octets spécifiés.
	
	Les entiers sont tronqués à l'octet, et assemblés en un vecteur de type unsigned char.
	Les nombres ayant une valeur négative sont ignorés lors de la composition du message.
	*/
	void sendMessage(int first, int second=-1, int third=-1);
	
	/**
	\brief	Envoi le message MIDI.
	*/
	void sendMessage(QVector<unsigned char> message);
	
	signals:
	/**
	\brief	Est émis lors de la réception d'un signal MIDI.
	
	Seuls les messages MIDI allant jusqu'à trois octets sont transmis par ce signal.
	\e second et \e third valent -1 lorsque le message ne les spécifie pas.
	*/
	void messageReceived(int first, int second, int third);
	
	protected slots:
	/**
	\brief	Gère la réception de message MIDI.
	*/
	virtual void receivesMessage(QVector<unsigned char> message) = 0;
	
	
	private:
	///Connexion.
	MidiConnection *m_connection;
	
	private slots:
	///Gère la réception de message MIDI par la connexion.
	void messageProcessing(QVector<unsigned char> message);
	
	///Change le statut et le texte informatif pour indiquer la connexion.
	void connectionStarted();
	///Change le statut et le texte informatif pour indiquer la déconnexion.
	void connectionFinished();
	
};

#endif /* __MIDIOBJECT_H__ */

