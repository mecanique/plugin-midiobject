<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="172"/>
        <source>Add the object “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="182"/>
        <source>Add the function “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="192"/>
        <source>Add the signal “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="202"/>
        <source>Add the variable “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecFunctionEditor</name>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="33"/>
        <source>Return type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="45"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="65"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="75"/>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="162"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecMidiNoteSignalEditor</name>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="24"/>
        <source>Note:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Do</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Do#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Ré</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Mi♭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Mi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Fa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Fa♯</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Sol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>La♭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>La</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Si♭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="26"/>
        <source>Si</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="28"/>
        <source>Octave:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="80"/>
        <source>MIDI signal editor error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiNoteSignalEditor.cpp" line="80"/>
        <source>&lt;h3&gt;MIDI note signal editor error&lt;/h3&gt;&lt;p&gt;An error is occured while the reading of the signal &quot;%1&quot;: no MIDI note specifications was found.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecMidiObjectEditor</name>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="39"/>
        <source>Select signal type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="40"/>
        <source>Select the signal type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="56"/>
        <source>Select MIDI event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="56"/>
        <source>Select the MIDI event:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="102"/>
        <source>No more MIDI note message available...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="102"/>
        <source>&lt;h3&gt;No more MIDI note message available&lt;/h3&gt;&lt;p&gt;All MIDI note messages (Note on, Note off and Polyphonic aftertouch) are defined, it is not possible to append a new signal.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="126"/>
        <source>No more MIDI program change message available...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="126"/>
        <source>&lt;h3&gt;No more MIDI program change message available&lt;/h3&gt;&lt;p&gt;All MIDI program change messages are defined, it is not possible to append a new signal.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="143"/>
        <source>No more MIDI message available...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="143"/>
        <source>&lt;h3&gt;No more MIDI message of this type is available&lt;/h3&gt;&lt;p&gt;All MIDI messages of this type are defined, it is not possible to append a new signal.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiObjectEditor.cpp" line="154"/>
        <location filename="../MecMidiObjectEditor.cpp" line="160"/>
        <source>Add the signal “%1”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecMidiObjectPlugin</name>
    <message>
        <location filename="../MecMidiObjectPlugin.cpp" line="32"/>
        <source>Manage the Musical Instrument Digital Interface protocol.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecMidiProgramChangeSignalEditor</name>
    <message>
        <location filename="../MecMidiProgramChangeSignalEditor.cpp" line="24"/>
        <source>Program:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiProgramChangeSignalEditor.cpp" line="56"/>
        <source>MIDI signal editor error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiProgramChangeSignalEditor.cpp" line="56"/>
        <source>&lt;h3&gt;MIDI note signal editor error&lt;/h3&gt;&lt;p&gt;An error is occured while the reading of the signal &quot;%1&quot;: no MIDI program change specifications was found.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecMidiSignalEditor</name>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="26"/>
        <source>Channel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="32"/>
        <source>Event:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Note off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Note on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Polyphonic aftertouch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Program change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Channel aftertouch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="34"/>
        <source>Pitch wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="90"/>
        <source>MIDI signal editor error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="90"/>
        <source>&lt;h3&gt;MIDI signal editor error&lt;/h3&gt;&lt;p&gt;An error is occured while the reading of the signal &quot;%1&quot; no MIDI specifications was found.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MecMidiSignalEditor.cpp" line="135"/>
        <source>Change name of “%1” to “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="67"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="222"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="77"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="223"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="96"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="224"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecProjectEditor</name>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="30"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="35"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="45"/>
        <source>Synopsis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="56"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="66"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="76"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="265"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="86"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="266"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="105"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="267"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="407"/>
        <source>Change title of the project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="415"/>
        <source>Change synopsis of the project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="204"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="224"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="231"/>
        <source>Add connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="233"/>
        <source>Remove connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="241"/>
        <source>Connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="403"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
